"use strict";
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const UserSchema = new Schema({
    email: String,
    password: String
});
const User = mongoose.model('user', UserSchema, 'users');
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = User;
