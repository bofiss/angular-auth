"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const UserSchema = new Schema({
    email: String,
    password: String
});
const user = mongoose.model('user', UserSchema, 'users');
exports.default = UserSchema;
