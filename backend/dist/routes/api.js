"use strict";
const express = require('express');
const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');
const user_1 = require('../models/user');
const router = express.Router();
const db = "mongodb://admin:adminpass@ds157528.mlab.com:57528/message";
const eventsData = [
    {
        "_id": "1",
        "name": "Exposition 2018",
        "description": "Lorem ipsum dolor",
        "date": "2018-04-23T18:25:43.511Z"
    },
    {
        "_id": "2",
        "name": "Auto Expo",
        "description": "Lorem ipsum",
        "date": "2018-12-23T18:25:43.511Z"
    },
    {
        "_id": "3",
        "name": "Wold Expo 2018",
        "description": "Lorem ipsum dolor",
        "date": "2018-06-23T18:25:43.511Z"
    },
    {
        "_id": "4",
        "name": "Auto Expo",
        "description": "Lorem sit amet",
        "date": "2018-10-23T18:25:43.511Z"
    },
    {
        "_id": "1",
        "name": "Exposition 2018",
        "description": "Lorem ipsum dolor",
        "date": "2018-04-23T18:25:43.511Z"
    },
    {
        "_id": "2",
        "name": "Auto Expo",
        "description": "Lorem ipsum",
        "date": "2018-12-23T18:25:43.511Z"
    },
    {
        "_id": "3",
        "name": "Wold Expo 2018",
        "description": "Lorem ipsum dolor",
        "date": "2018-06-23T18:25:43.511Z"
    },
    {
        "_id": "4",
        "name": "Auto Expo",
        "description": "Lorem sit amet",
        "date": "2018-10-23T18:25:43.511Z"
    }
];
mongoose.connect(db, (err) => {
    if (err) {
        console.error(err);
    }
    else {
        console.log('Connected to mongodb');
    }
});
function verifyToken(req, res, next) {
    if (!req.headers.authorization) {
        return res.status(401).send('Unauthorized request');
    }
    let token = req.headers.authorization.split(' ')[1];
    if (token === 'null') {
        return res.status(401).send('Unauthorized request');
    }
    let payload = jwt.verify(token, 'secretkey');
    if (!payload) {
        return res.status(401).send('Unauthorized request');
    }
    req.userId = payload.subject;
    next();
}
router.get('/', (req, res) => {
    res.send('From API route');
});
router.post('/register', (req, res) => {
    const userData = req.body;
    let user = new user_1.default(userData);
    user.save((error, registeredUser) => {
        if (error) {
            console.log(error);
        }
        else {
            let payload = { subject: registeredUser._id };
            let token = jwt.sign(payload, 'secretkey');
            res.status(200).send({ token: token });
        }
    });
});
router.post('/login', (req, res) => {
    const userData = req.body;
    user_1.default.findOne({ email: userData.email }, (err, user) => {
        if (err) {
            console.log(err);
        }
        else {
            if (!user) {
                res.status(401).send('Invalid email');
            }
            else {
                if (user.password !== userData.password) {
                    res.status(401).send('Invalid password');
                }
                else {
                    let payload = { subject: user._id };
                    let token = jwt.sign(payload, 'secretkey');
                    res.status(200).send({ token: token });
                }
            }
        }
    });
});
router.get('/events', (req, res) => {
    res.json(eventsData);
});
router.get('/special', verifyToken, (req, res) => {
    res.json(eventsData);
});
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = router;
