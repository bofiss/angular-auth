"use strict";
const express = require('express');
const BodyParser = require('body-parser');
const api_1 = require('./routes/api');
const cors = require('cors');
const app = express();
app.use(cors());
const PORT = 5000;
app.use(BodyParser.json());
app.use('/api', api_1.default);
app.get('/', (req, res) => {
    res.send('hello from server');
});
app.listen(PORT, () => {
    console.log('running on port ' + PORT);
});
