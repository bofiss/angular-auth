import * as mongoose from "mongoose"

const Schema = mongoose.Schema

const UserSchema =  new Schema({
     email: String,
     password: String
})

const User = mongoose.model<any>('user', UserSchema, 'users')

export default User