import * as express from 'express'
import * as BodyParser from 'body-parser'
import router from './routes/api'
import * as cors from 'cors'

const app = express()
app.use(cors())
const PORT = 5000

app.use(BodyParser.json())
app.use('/api', router)

app.get('/', (req, res) => {
    res.send('hello from server')
})

app.listen(PORT, () => {
    console.log('running on port '+ PORT)
})